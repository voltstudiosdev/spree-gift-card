task create_credits_for_users: :environment do
    Spree::User.all.each do |user|
       if !user.credit
          Spree::Credit.create(user: user, amount: 0) 
       else
           if !user.credit.amount
               user.credit.amount = 0
           end
       end
    end
end