require 'spec_helper'

RSpec.describe GiftCard, type: :model do
  describe "#create" do
    it "generates a random giftcard code" do
      giftCard = GiftCard.create
      
      expect(giftCard.card).to be_kind_of(String)
      expect(giftCard.card.length).to eq(14)
    end
  end
  
  describe "#claim" do
    it "sets the claimer to be the user with the id sent as param" do
      user = Spree::User.new(id: 1)
      user.save
      #user = Spree::User.find(1)
      #giftCard = GiftCard.create
      #giftCard.claim(user.id)
      
      expect(Spree::User.find(1)).not_to be_false
    end
  end
end
