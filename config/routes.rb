Spree::Core::Engine.routes.draw do
  # Add your extension routes here
    # Example of regular route:
  #   get 'products/:id' => 'catalog#view'
  namespace :admin do
    resources :credits
    resources :users do
      resource :credits
    end
  end
end
