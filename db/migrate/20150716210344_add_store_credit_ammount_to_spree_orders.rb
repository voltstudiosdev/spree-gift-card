class AddStoreCreditAmmountToSpreeOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :store_credit_ammount, :decimal
  end
end
