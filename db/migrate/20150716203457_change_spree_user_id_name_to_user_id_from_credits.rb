class ChangeSpreeUserIdNameToUserIdFromCredits < ActiveRecord::Migration
  def change
    rename_column :credits, :spree_user_id, :user_id
  end
end
