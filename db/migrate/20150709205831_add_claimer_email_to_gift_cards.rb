class AddClaimerEmailToGiftCards < ActiveRecord::Migration
  def change
    add_column :gift_cards, :claimer_email, :string
  end
end
