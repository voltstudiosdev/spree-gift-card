class RenameCreditsToSpreeCredits < ActiveRecord::Migration
  def change
    rename_table :credits, :spree_credits
  end
end
