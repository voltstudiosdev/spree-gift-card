class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.decimal :amount
      t.references :spree_user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
