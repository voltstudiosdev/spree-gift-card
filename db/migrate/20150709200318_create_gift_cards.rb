class CreateGiftCards < ActiveRecord::Migration
  def change
    create_table :gift_cards do |t|
      t.string :card
      t.string :buyer_email
      t.decimal :ammount
      t.boolean :claimed

      t.timestamps null: false
    end
  end
end
