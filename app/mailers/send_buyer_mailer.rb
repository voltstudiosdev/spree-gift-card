class SendBuyerMailer < ApplicationMailer
  default from: ENV["email_address"]
  
  def gift_card_mail(card)
    @card = card
    mail(to: card.buyer_email, subject: "Gift Card Purchase")
  end
end
