require 'securerandom'

class GiftCard < ActiveRecord::Base
  before_validation :generate_gift_code
  after_save :send_buyer_email
  
  belongs_to :claimer, class_name: 'Spree::User', inverse_of: :gift_cards
  
  def claim (claimer_id)
    self.claimer = Spree::User.find(claimer_id)
    self.claimer.credit.amount += self.ammount
    self.claimer.credit.save
    self.claimed = true
  end
  
  private
  
  def generate_gift_code
    code = SecureRandom.hex(7)
    while (GiftCard.exists?(card: code))
      code = SecureRandom.hex(7)
    end
    
    self.card = code
  end
  
  def send_buyer_email
    SendBuyerMailer.gift_card_mail(self).deliver
  end
end
