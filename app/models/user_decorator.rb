Spree::User.class_eval do
  has_many :gift_cards, class_name: 'GiftCard', inverse_of: :claimer
  has_one :credit, class_name: 'Spree::Credit', inverse_of: :user
  
  after_create :create_user_credit
  
  private
  
    def create_user_credit
      Spree::Credit.create(user: self, amount: 0)
    end
  
end