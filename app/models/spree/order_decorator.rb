Spree::Order.insert_checkout_step :store_credit, before: :payment
Spree::Order.state_machine.before_transition from: :store_credit, do: :create_credit_adjustment

# Spree::Order.state_machine.after_transition to: :complete, do: :create_gift_cards
Spree::Order.state_machine.after_transition to: :complete, do: :update_user_credit

Spree::PermittedAttributes.checkout_attributes << :store_credit_ammount

def create_gift_cards
  self.line_items.each do |item|
    if item.product.name == "Gift Card"
      item.quantity.times do
        GiftCard.create(ammount: item.price, buyer_email: self.email)
      end
    end
  end
end

def create_credit_adjustment
  if !has_gift_cards? && self.user && self.user.credit && self.user.credit.amount && self.user.credit.amount > 0
    credit_used = self.store_credit_ammount ? self.store_credit_ammount.abs : 0
    store_credit_adjustments = self.adjustments.where(label: "Store Credit")
    
    store_credit_adjustments.delete_all if store_credit_adjustments.count > 1
    
    if credit_used <= [self.user.credit.amount, self.amount].min
      if store_credit_adjustments.count == 1
        store_credit_adjustments.first.update(amount: credit_used * -1)
      else
        Spree::Adjustment.create(amount: credit_used * -1,
          label: "Store Credit",
          adjustable_type: "Spree::Order",
          adjustable_id: self.id,
          eligible: true,
          state: "open",
          order_id: self.id,
          included: false)
      end
    end
  end
end

def update_user_credit
  if has_gift_cards?
    self.adjustments.where(label: "Store Credit").delete_all
    self.store_credit_ammount = 0;
  else 
    if self.user && self.user.credit && self.user.credit.amount && self.user.credit.amount > 0
      credit_used = self.store_credit_ammount
      initial_credit = self.user.credit.amount
      self.user.credit.update(amount: initial_credit - credit_used)
    end
  end
end

public

def has_gift_cards?
  result = false
  line_items.each do |item|
    if item.product.name == "Gift Card"
      result = true
      break
    end
  end
  result
end
