module Spree
  class Credit < Spree::Base
    belongs_to :user, class_name: "Spree::User", inverse_of: :credit
  end
end
