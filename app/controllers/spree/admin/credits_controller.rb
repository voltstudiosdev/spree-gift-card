module Spree
  class Admin::CreditsController < Admin::ResourceController
    def index
        redirect_to '/admin/users'
    end
  end
end